using BioSequences
using CSV
using DataFrames
using ARTEMIS

dir = dirname(abspath(Base.source_path()))

df = DataFrame(CSV.File(dir * "/../data/Pan_2022/table_surro_extended.csv"))
insertcols!(df, :COH_offt_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_guide_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_lev => Vector{Union{Int, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_hamm => Vector{Union{Int, Missing}}(missing, nrow(df)))

for r in eachrow(df)
    aln = ARTEMIS.align(
        LongDNA{4}(reverse(String(r["RGN spacer sequences"]))), 
        LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][begin:end-3]))), 7)
    r["COH_offt_aln"] = String(reverse(aln.ref))
    r["COH_guide_aln"] = String(reverse(aln.guide))
    r["COH_lev"] = aln.dist + ('G' != r["offtarget_sequence_extended"][end]) +
    ('G' != r["offtarget_sequence_extended"][end-1])
    r["COH_hamm"] = sum(LongDNA{4}(reverse(String(r["RGN spacer sequences"]))) .!= LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][8:end-3])))) + ('G' != r["offtarget_sequence_extended"][end]) +
        ('G' != r["offtarget_sequence_extended"][end-1])
end
CSV.write(dir * "/../data/Pan_2022/table_surro_extended_with_COH.csv", df)

df = DataFrame(CSV.File(dir * "/../data/Rongjie_Fu_2022/table_5_extended.csv"))
insertcols!(df, :COH_offt_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_guide_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_lev => Vector{Union{Int, Missing}}(missing, nrow(df)))
for r in eachrow(df)
    aln = ARTEMIS.align(
        LongDNA{4}(reverse(String(r["sgRNA sequence"]))), 
        LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][begin:end-3]))), 7)
    r["COH_offt_aln"] = String(reverse(aln.ref))
    r["COH_guide_aln"] = String(reverse(aln.guide))
    r["COH_lev"] = aln.dist + ('G' != r["offtarget_sequence_extended"][end]) +
        ('G' != r["offtarget_sequence_extended"][end-1])
end
CSV.write(dir * "/../data/Rongjie_Fu_2022/table_6_extended_with_COH.csv", df)

df = DataFrame(CSV.File(dir * "/../data/Lazzarotto_2020/table_3_extended.csv"))
insertcols!(df, :COH_offt_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_guide_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_lev => Vector{Union{Int, Missing}}(missing, nrow(df)))
for r in eachrow(df)
    aln = ARTEMIS.align(
        LongDNA{4}(reverse(String(r["target"][begin:end-3]))), 
        LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][begin:end-3]))), 7)
    r["COH_offt_aln"] = String(reverse(aln.ref))
    r["COH_guide_aln"] = String(reverse(aln.guide))
    r["COH_lev"] = aln.dist + (r["target"][end] != r["offtarget_sequence_extended"][end]) +
        (r["target"][end-1] != r["offtarget_sequence_extended"][end-1]) # this accounts for the mismatch in PAM
end
CSV.write(dir * "/../data/Lazzarotto_2020/table_3_extended_with_COH.csv", df)

df = DataFrame(CSV.File(dir * "/../data/Lazzarotto_2020/table_6_extended.csv"))
insertcols!(df, :COH_offt_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_guide_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_lev => Vector{Union{Int, Missing}}(missing, nrow(df)))
for r in eachrow(df)
    aln = ARTEMIS.align(
        LongDNA{4}(reverse(String(r["target"][begin:end-3]))), 
        LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][begin:end-3]))), 7)
    r["COH_offt_aln"] = String(reverse(aln.ref))
    r["COH_guide_aln"] = String(reverse(aln.guide))
    r["COH_lev"] = aln.dist + (r["target"][end] != r["offtarget_sequence_extended"][end]) +
        (r["target"][end-1] != r["offtarget_sequence_extended"][end-1])
end
CSV.write(dir * "/../data/Lazzarotto_2020/table_6_extended_with_COH.csv", df)
