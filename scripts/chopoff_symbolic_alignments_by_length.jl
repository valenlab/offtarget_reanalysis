using CHOPOFF
using BioSequences
using StatsBase
using DataFrames
using CSV

motif = Motif("Cas9"; distance = 4)

# distances + lengths + sa + applied_alignments?

storagepath = dirname(@__FILE__)
storagepath = joinpath(storagepath, "../data/path_templates.bin")
#mpt = build_PathTemplates(Motif("Cas9"; distance = 1)) # compiling
#mpt = build_PathTemplates(motif; storagepath = storagepath, restrict_to_len = 20)
mpt = CHOPOFF.load(storagepath)
paths = mpt.paths
distances = mpt.distances

paths = paths[:, 1:16]
not_dups = map(!, BitVector(nonunique(DataFrame(paths, :auto))))
paths = paths[not_dups, :]
distances = distances[not_dups]

paths = UInt8.(paths)
distances = UInt8.(distances)
CHOPOFF.save(distances, "./data/Cas9_d4_p16_distances.bin")
CHOPOFF.save(paths, "./data/Cas9_d4_p16_paths.bin")

d2 = CHOPOFF.load("./data/Cas9_d4_p16_distances.bin")
p2 = CHOPOFF.load("./data/Cas9_d4_p16_paths.bin")
if (paths != p2) | (distances != d2)
    @warn "Failed to sucessfully save the path templates. CHOPOFF will still work, but will be slower in some cases."
    rm("./data/Cas9_d4_p16_paths.bin")
    rm("./data/Cas9_d4_p16_distances.bin")
end

len = []
dist = []
counts = []
avg = []
for i in 20:-1:1
    @info i
    # reducing the i
    paths = paths[:, 1:i]
    not_dups = map(!, BitVector(nonunique(DataFrame(paths, :auto))))
    paths = paths[not_dups, :]
    distances = distances[not_dups]
    cm = countmap(distances)

    append!(len, repeat([i], length(keys(cm))))
    append!(dist, keys(cm))
    append!(counts, values(cm))

    # now we can do average guide and the counts for them
    # for j in 1:100000
end

dt = DataFrame(prefix = len, distance = dist, counts = counts)
CSV.write(joinpath(dirname(storagepath), "prefix_distance_counts.csv"), dt)