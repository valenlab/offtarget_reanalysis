using ARTEMIS
using BioSequences
using CSV
using DataFrames

#genome = "/home/ai/Projects/uib/CRISPR/offtarget/artemis-benchmark/data/hg38v34.fa"
#db_path = "/home/ai/Projects/uib/CRISPR/offtarget/artemis-benchmark/out_dir/artemis_out/db/linearDB_9_4/"
#out_dir = "/home/ai/Projects/uib/CRISPR/offtarget/offtarget_reanalysis/data/top_genes/"

genome = "/home/ai/artemis-benchmark/data/hg38v34.fa"
db_path = "/home/ai/artemis-benchmark/out_dir/artemis_out/db/linearDB_9_4/"
out_dir = "/home/ai/offtarget_reanalysis/data/top_genes/"

# build DB
#build_linearDB(
#    "hg38v34_Cas9_prefix_9", genome, 
#    Motif("Cas9"), 
#    db_path, 
#    9)

# search
dt = DataFrame(CSV.File("/home/ai/offtarget_reanalysis/data/all_genes.csv"))
genes = unique(dt[!, "gene"])
res_all = DataFrame()

for (i, gene) in enumerate(genes) 
    @info string(i) * "  " * gene

    res_dir = joinpath(out_dir, gene)
    if !isdir(res_dir) 
        mkdir(res_dir)
    end
    res_path = joinpath(res_dir, gene * ".csv")
    res_path_summary = joinpath(res_dir, gene * "_summary.csv")
    guides = LongDNA{4}.(Set(SubString.(filter(:gene => ==(gene), dt)[!, "Target sequence"], 1, 20))) 
    
    if !isfile(res_path)
        search_linearDB(db_path, guides, res_path; distance = 2) 
        res = DataFrame(CSV.File(res_path))
        res = filter_overlapping(res, 2)
        res = summarize_offtargets(res, 2)
        res[!, :gene] .= gene
        append!(res_all, res)
    end
end
CSV.write(joinpath(out_dir, "all_top_genes_summary.csv"), res_all)
